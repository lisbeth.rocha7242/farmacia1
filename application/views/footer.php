<div class="well well-lg">Desarrollado Por:Lisbeth Rocha
  <!-- Footer -->
  <footer class="text-center text-lg-start bg-light text-muted">
    <!-- Section: Social media -->
    <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
      <!-- Left -->
      <div class="me-5 d-none d-lg-block">
        <span>Para Todos Nuestros Clientes</span>
      </div>
      <!-- Left -->

      <!-- Right -->
      <div>
        <a href="" class="me-4 text-reset">
          <i class="fab fa-facebook-f"></i>
        </a>
        <a href="" class="me-4 text-reset">
          <i class="fab fa-twitter"></i>
        </a>
        <a href="" class="me-4 text-reset">
          <i class="fab fa-google"></i>
        </a>
        <a href="" class="me-4 text-reset">
          <i class="fab fa-instagram"></i>
        </a>
        <a href="" class="me-4 text-reset">
          <i class="fab fa-linkedin"></i>
        </a>
        <a href="" class="me-4 text-reset">
          <i class="fab fa-github"></i>
        </a>
      </div>
      <!-- Right -->
    </section>
    <!-- Section: Social media -->

    <!-- Section: Links  -->
    <section class="">
      <div class="container text-center text-md-start mt-5">
        <!-- Grid row -->
        <div class="row mt-3">
          <!-- Grid column -->
          <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
            <!-- Content -->
            <h6 class="text-uppercase fw-bold mb-4">
              <i class="fas fa-gem me-3"></i><b>Sana Sana Es Mejor<b>
            </h6>
            <p>
              SanaSana, unidad de negocio de Corporación GPF se fundó en el año 2000. Concebida como la cadena de farmacias propias más grande del país.
            </p>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              Productos
            </h6>
            <p>
              <a href="#!" class="text-reset">Medicinas</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Articulos de Bebé</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Visuteria</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Snacks</a>
            </p>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              Equipo
            </h6>
            <p>
              <a href="#!" class="text-reset">Marco Perez</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Rosita Jacome</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Jorge Ramos</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Maria Paredes</a>
            </p>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
            <p><i class="fas fa-home me-3"></i> Latacunga - Barrio Ignacio Flores</p>
            <p>
              <i class="fas fa-envelope me-3"></i>
              sanasana@gmail.com
            </p>
            <p><i class="fas fa-phone me-3"></i> 0985678998</p>
            <p><i class="fas fa-print me-3"></i> 2230-259</p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->

    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
      © 2021 Copyright:
      <a class="text-reset fw-bold" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->
</div>
</body>
</html>
