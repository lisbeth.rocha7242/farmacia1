<div class="container-fluid bg-info text-white text-center">

<h1>HISTORIA</h1>
<div class="row">
  <div class="col-md-4">

    <h4 class="text-center">¿Quienes Somos?</h4>
    <img src="<?php echo base_url(); ?>/assets/image/noso.png" height="150" width="200">
    <p class="text-justify"> “Alivio en todo sentido”
SanaSana, unidad de negocio de Corporación GPF se fundó en el año 2000. Concebida como la cadena de farmacias propias más grande del país, está presente en las 24 provincias del Ecuador, contribuyendo al bienestar y la salud de cientos de comunidades y barrios.</p>
  </div>
  <div class="col-md-4">

    <h4 class="text-center">Misión</h4>
    <img src="<?php echo base_url(); ?>/assets/image/mision.png" height="150" width="200">
        <p class="text-justify">Ser reconocidos por brindar a nuestros clientes sensaciones agradables y momentos felices.
        Posicionarnos en el corazón de las familias palmiranas y de todos los que nos visitan.
        Contribuir y aportar nuestro granito de arena, para generar una colombia feliz y en paz; que
        brinde un mejor futuro a nuestras próximas generaciones.</p>
  </div>
  <div class="col-md-4">

    <h4 class="text-center">Visión</h4>
    <img src="<?php echo base_url(); ?>/assets/image/vision.png" height="150" width="200">
        <p class="text-justify">Tenemos un compromiso con la salud, la educación y el bienestar de la comunidad. Junto a nuestros aliados, Cruz Roja Ecuatoriana y la Junta de Beneficencia de Guayaquil, desarrollamos iniciativas en los sectores más vulnerables del país. Desde el 2010 hemos beneficiado a más de 162.000 personas a nivel nacional. El campo de acción de Corporación GPF y su marca SanaSana se alinea con los Objetivos de Desarrollo Sostenible (ODS) de las Naciones Unidas, impactando de manera directa en el 52% de los 17 ODS.</p>
  </div>
</div>
 </div>
