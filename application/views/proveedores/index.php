<div class="row">
  <div class="col-md-8">
    <h1>Listado de Proveedores Excelentes</h1>
  </div>
  <br>
  <div class="col-md-4">
     <a href="<?php echo site_url('proveedores/nuevo'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
    Agregar Proveedores</a>
  </div>
</div>
<br>
<?php if ($proveedores): ?>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRES</th>
        <th>APELLIDO</th>
        <th>EDAD</th>
        <th>DIRECCIÓN</th>
        <th>TELÉFONO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($proveedores as $filaTemporal): ?>
        <tr>
          <td>
           <?php echo $filaTemporal->id_pro ?>
          </td>

          <td>
           <?php echo $filaTemporal->nombre_pro ?>
          </td>
          <td>
           <?php echo $filaTemporal->apellido_pro ?>
          </td>
          <td>
           <?php echo $filaTemporal->edad_pro ?>
          </td>
          <td>
           <?php echo $filaTemporal->direccion_pro ?>
          </td>
          <td>
           <?php echo $filaTemporal->telefono_pro ?>
          </td>


        <td class="text-center">
          <a href="#" title="Editar Proveedor" style="color:blue">
          <i class="glyphicon glyphicon-pencil"></i>
          </a>
          &nbsp; &nbsp; &nbsp;
          <a href="<?php echo site_url(); ?>/proveedores/eliminar/<?php echo $filaTemporal->id_pro;?>" title="Eliminar Proveedor" onclick="return confirm('¿Estas Seguro de eliminar el registro proveedores?');"style="color:red">
          <i class="glyphicon glyphicon-trash"></i>
          </a>
        </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>No hay Datos</h1>
<?php endif; ?>
