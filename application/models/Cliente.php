<?php
class cliente extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor un instructor en mysql
  function insertar($datos){

  return $this->db->insert("cliente",$datos);
  }
  //FUNCION PARA CONSULTAR Instructores
  function obtenerTodos(){
    $listadoClientes=
    $this->db->get("cliente");
    if($listadoClientes->num_rows()>0){//Si hay datos
        return $listadoClientes->result();
    } else{ //No hay datos
       return false;
    }
  }
  //BORRAR
  function borrar($id_cli){
    $this->db->where("id_cli",$id_cli);
    if ($this->db->delete("cliente")) {
      return true;
    } else {
      return false;
    }

  }
}//cierre de la clase


 ?>
