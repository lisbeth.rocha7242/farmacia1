<h1>NUEVOS CLIENTES</h1>
<form class=""
action="<?php echo site_url(); ?>/Clientes/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la cédula"
          class="form-control"
          name="cedula_cli" value=""
          id="cedula_cli">

      </div>
      <div class="col-md-4">
          <label for="">Primer Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el primer apellido"
          class="form-control"
          name="primer_apellido_cli" value=""
          id="primer_apellido_cli">
      </div>
      <div class="col-md-4">
        <label for="">Segundo Apellido:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el segundo apellido"
        class="form-control"
        name="segundo_apellido_cli" value=""
        id="segundo_apellido_cli">
      </div>
    </div>
    <br>

    <div class="row">
      <div class="col-md-4">
          <label for="">Nombres:</label>
          <br>
          <input type="text"
          placeholder="Ingrese Sus Nombres"
          class="form-control"
          name="nombres_cli" value=""
          id="nombres_cli">
      </div>
      <div class="row">
        <div class="col-md-4">
            <label for="">Direccion:</label>
            <br>
            <input type="text"
            placeholder="Ingrese la direccion"
            class="form-control"
            name="direccion_cli" value=""
            id="direccion_cli">

        </div>

    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url();?>/clientes/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
