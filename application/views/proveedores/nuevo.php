<h1>NUEVO PROVEEDOR</h1>
<form class=""
action="<?php echo site_url(); ?>/proveedores/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su Nombre"
          class="form-control"
          name="nombre_pro" value=""
          id="nombre_pro">

      </div>
      <div class="col-md-4">
          <label for="">Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido"
          class="form-control"
          name="apellido_pro" value=""
          id="apellido_pro">
      </div>
      <div class="col-md-4">
        <label for="">Edad:</label>
        <br>
        <input type="text"
        placeholder="Ingrese su Edad"
        class="form-control"
        name="edad_pro" value=""
        id="edad_pro">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Dirección:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su Direccion"
          class="form-control"
          name="direccion_pro" value=""
          id="direccion_pro">
      </div>

      <div class="col-md-4">
        <label for="">Teléfono:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el telefono"
        class="form-control"
        name="telefono_pro" value=""
        id="telefono_pro">
      </div>
    </div>


    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/proveedores/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
