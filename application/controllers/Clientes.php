<?php

    class Clientes extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Cliente');

        }

        //Funcion que renderiza la vista index

        public function index(){

            $data['clientes']=$this->Cliente->obtenerTodos();
            $this->load->view('header');
            $this->load->view('clientes/index',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevos(){
            $this->load->view('header');
            $this->load->view('clientes/nuevos');
            $this->load->view('footer');
        }

        public function guardar(){
        $datosNuevosClientes=array("cedula_cli"=>$this->input->post('cedula_cli'),"primer_apellido_cli"=>$this->input->post('primer_apellido_cli'),
        "segundo_apellido_cli"=>$this->input->post('segundo_apellido_cli'),"nombres_cli"=>$this->input->post('nombres_cli'),"direccion_cli"=>$this->input->post('direccion_cli')
      );
        if($this->Cliente->insertar($datosNuevosClientes)){
          redirect('clientes/index');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_cli){
       if ($this->Cliente->borrar($id_cli)){
         redirect('clientes/index');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
