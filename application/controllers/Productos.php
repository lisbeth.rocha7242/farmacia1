<?php

    class Productos extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Producto');

        }

        //Funcion que renderiza la vista index

        public function index(){
          $data['productos']=$this->Producto->obtenerTodos();

            $this->load->view('header');
            $this->load->view('productos/index',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('productos/nuevo');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosProducto=array("nombre_pro"=>$this->input->post('nombre_pro'),"cantidad_pro"=>$this->input->post('cantidad_pro'),
          "precio_pro"=>$this->input->post('precio_pro'),"categoria_pro"=>$this->input->post('categoria_pro')
        );if($this->Producto->insertar($datosNuevosProducto)){
          redirect('productos/index');
        }
        else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_pro){
       if ($this->Producto->borrar($id_pro)){
         redirect('productos/index');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
