<?php
class Producto extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor un instructor en mysql
  function insertar($datos){

  return $this->db->insert("producto",$datos);
  }

  //FUNCION PARA CONSULTAR Instructores
  function obtenerTodos(){
    $listadoProductos=$this->db->get("producto");
    if($listadoProductos->num_rows()>0){//Si hay datos
        return $listadoProductos->result();
    } else{ //No hay datos
       return false;
    }
  }
  //BORRAR
  function borrar($id_pro){
    $this->db->where("id_pro",$id_pro);
    if ($this->db->delete("producto")) {
      return true;
    } else {
      return false;
    }

  }
}//cierre de la clase
 ?>
