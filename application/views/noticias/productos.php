<div class="container-fluid bg-info text-white text-center">
<h1 class="text-center" >PRODUCTOS</h1>
  <div class="row">

        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p9.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Kaloba gotas</h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i>Precio:$7,15</p>
              <a href="#" class="btn btn-primary">  Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/PUeaHfC.png">
              <div class="zoom"></div>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p10.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Muxelix Jarabe</h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i> Precio:$5,99</p>
              <a href="#" class="btn btn-primary">Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/PUeaHfC.png">
              <div class="zoom"></div>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p11.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Muxol Adulto </h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i>Precio:$ 3,99</p>
              <a href="#" class="btn btn-primary">Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/8cOtAS9.png">
              <div class="zoom"></div>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p13.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Broncotosil 8 mg Cápsulas</h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i> Precio:$6,22</p>
              <a href="#" class="btn btn-primary">Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/p7OYoBT.png">
              <div class="zoom"></div>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p16.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Vaporal Expectorante jarabe </h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i>Precio:$ 12</p>
              <a href="#" class="btn btn-primary">Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/uZeTibF.png">
              <div class="zoom"></div>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p22.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Propóleo Green Life Menta </h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i>Precio:$ 7.99</p>
              <a href="#" class="btn btn-primary">Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/ymwlLln.png">
              <div class="zoom"></div>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p23.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Dextrin G jarabe </h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i> Precio:$12</p>
              <a href="#" class="btn btn-primary">Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/PUeaHfC.png">
              <div class="zoom"></div>
            </a>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12 single_portfolio_text">
            <img src="https://www.sanasana.com.ec/PreciosDeLocura/images/p29.png" alt="" />
            <div class="portfolio_images_overlay text-center">
              <h6 class="clrd-font">Ensoy Adultos Vainilla </h6>
              <p class="clrd-font product_price"> <i class="fa fa-usd clrd-font" aria-hidden="true"></i> Precio:$18</p>
              <a href="#" class="btn btn-primary">Comprar</a>
            </div>
            <a class="fancybox" rel="ligthbox" href="http://i.imgur.com/A8FRrbS.png">
              <div class="zoom"></div>
            </a>
        </div>

  </div>
</div>
