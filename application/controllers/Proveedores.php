<?php

    class Proveedores extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Proveedor');

        }

        //Funcion que renderiza la vista index

        public function index(){
          $data['proveedores']=$this->Proveedor->obtenerTodos();

            $this->load->view('header');
            $this->load->view('proveedores/index',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('proveedores/nuevo');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosProveedor=array("nombre_pro"=>$this->input->post('nombre_pro'),"apellido_pro"=>$this->input->post('apellido_pro'),
          "edad_pro"=>$this->input->post('edad_pro'),"direccion_pro"=>$this->input->post('direccion_pro'),"telefono_pro"=>$this->input->post('telefono_pro')
        );if($this->Proveedor->insertar($datosNuevosProveedor)){
          redirect('proveedores/index');
        }
        else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_ins){
       if ($this->Proveedor->borrar($id_ins)){
         redirect('proveedores/index');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
