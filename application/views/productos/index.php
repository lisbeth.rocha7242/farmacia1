<div class="row">
  <div class="col-md-8">
    <h1>Listado de Productos</h1>
  </div>
  <br>
  <div class="col-md-4">
    <a href="<?php echo site_url('productos/nuevo'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
    Agregar Productos</a>
  </div>
</div>
<br>
<?php if ($productos): ?>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>CANTIDAD</th>
        <th>PRECIO</th>
        <th>CATEGORÍA/th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($productos as $filaTemporal): ?>
        <tr>
          <td>
           <?php echo $filaTemporal->id_pro ?>
          </td>

          <td>
           <?php echo $filaTemporal->nombre_pro ?>
          </td>
          <td>
           <?php echo $filaTemporal->cantidad_pro ?>
          </td>
          <td>
           <?php echo $filaTemporal->precio_pro ?>
          </td>
          <td>
           <?php echo $filaTemporal->categoria_pro ?>
          </td>



        <td class="text-center">
          <a href="#" title="Editar Producto" style="color:blue">
          <i class="glyphicon glyphicon-pencil"></i>
          </a>
          &nbsp; &nbsp; &nbsp;
          <a href="<?php echo site_url(); ?>/productos/eliminar/<?php echo $filaTemporal->id_pro;?>" title="Eliminar Producto" onclick="return confirm('¿Estas Seguro de eliminar el registro productos?');"style="color:red">
          <i class="glyphicon glyphicon-trash"></i>
          </a>
        </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>No hay Datos</h1>
<?php endif; ?>
