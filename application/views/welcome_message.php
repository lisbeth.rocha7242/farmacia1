<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?php echo base_url(); ?>/assets/image/sana2.png" width="1000" height="3000">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/image/sana.png"  width="1400" height="3000">
    </div>

    <div class="item">
      <img src="<?php echo base_url(); ?>/assets/image/sana3.png" width="1400" height="3000">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="container-fluid bg-info text-white text-center">
      <h1><b>BIENVENIDOS A FARMACIAS SANA SANA<b></h1>
      <div class="container mt-5">
        <div class="row">
          <div class="col-sm-4">
            <h3><b>Promociones<b></h3>
            <p>La promoción se suele relacionar con la comunicación del producto o servicio. Engloba todo aquello que sirve para estimular la compra o venta de un producto/servicio.</p>
          </div>
          <div class="col-sm-4">
            <h3><b>Descuentos<b></h3>
            <p>Una “cambiatón” es la oportunidad para que un cliente renueve algo viejo. Se puede usar para diferentes productos como electrodomésticos, utensilios de cocina, consumibles </p>

          </div>
          <div class="col-sm-4">
            <h3><b>Ubicación<b></h3>
              <div clas="col-md-8">
            <div id ="mapaDireccion" style="width:100%; height:300px">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.2809589360363!2d-78.61483772615243!3d-0.940370899050541!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91d461a0bcc134cd%3A0x4e077ef1b0c5c3a1!2sSana%20Sana!5e0!3m2!1ses!2sec!4v1685592236216!5m2!1ses!2sec" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

            </div>
       </div>

          </div>
        </div>
      </div>

</div>
