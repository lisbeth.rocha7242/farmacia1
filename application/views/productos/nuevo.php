<h1>NUEVO PRODUCTOS</h1>
<form class=""
action="<?php echo site_url(); ?>/productos/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su Nombre"
          class="form-control"
          name="nombre_pro" value=""
          id="nombre_pro">

      </div>
      <div class="col-md-4">
          <label for="">Cantidad:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la Cantidad"
          class="form-control"
          name="cantidad_pro" value=""
          id="cantidad_pro">
      </div>
      <div class="col-md-4">
        <label for="">Precio:</label>
        <br>
        <input type="number"
        placeholder="Ingrese su Precio"
        class="form-control"
        name="precio_pro" value=""
        id="precio_pro">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Categoría:</label>
          <br>
          <input type="text"
          placeholder="Ingrese su Categoría"
          class="form-control"
          name="categoria_pro" value=""
          id="categoria_pro">
      </div>


    </div>


    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/productos/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
