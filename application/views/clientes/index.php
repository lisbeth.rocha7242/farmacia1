<div class="row">
  <div class="col-md-8">
    <h1>Listado de Clientes Frecuentes</h1>
  </div>
  <br>
  <div class="col-md-4">
    <a href="<?php echo site_url('clientes/nuevos'); ?>" class="btn btn-primary">
      <i class="glyphicon glyphicon-plus"></i>
    Agregar Clientes</a>
  </div>
</div>
<br>
<?php if ($clientes): ?>
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>CEDULA</th>
        <th>PRIMER APELLIDO</th>
        <th>SEGUNDO APELLIDO</th>
        <th>NOMBRES</th>
        <th>DIRECCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($clientes as $filaTemporal): ?>
        <tr>
          <td>
           <?php echo $filaTemporal->id_cli ?>
          </td>

          <td>
           <?php echo $filaTemporal->cedula_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->primer_apellido_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->segundo_apellido_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->nombres_cli ?>
          </td>
          <td>
           <?php echo $filaTemporal->direccion_cli ?>
          </td>


          <td class="text-center">
            <a href="#" title="Editar Cliente" style="color:blue">
            <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli;?>" title="Eliminar Cliente" onclick="return confirm('¿Estas Seguro de eliminar el registro clientes?');"style="color:red">
            <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>

        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>No hay Datos</h1>
<?php endif; ?>
