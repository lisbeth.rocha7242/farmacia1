<div class="container-fluid bg-info text-white text-center">
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <img src="https://static8.depositphotos.com/1000291/801/i/450/depositphotos_8010219-stock-photo-pharmaceutical-factory-worker.jpg" alt="" / width="500" height="350">
        <h5 class="card-title">Adriana Perez</h5>
        <p class="card-text">Con más de 25 años de experiencia, Farmaenlace marca la diferencia en la comercialización y cobertura de productos farmacéuticos y de consumo a más de 3.200 clientes, a través de nuestros asesores de ventas y equipo de telemercadeo con sede en cada ciudad del País.</p>
        <a href="#" class="btn btn-primary">Llamar</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <img src="https://www.farmaenlace.com/wp-content/uploads/2022/07/imagen-distribucion-bordes.png" alt="" /width="500" height="350">
        <h5 class="card-title">Teresa Benavides</h5>
        <p class="card-text">Con más de 25 años de experiencia, Farmaenlace marca la diferencia en la comercialización y cobertura de productos farmacéuticos y de consumo a más de 3.200 clientes, a través de nuestros asesores de ventas y equipo de telemercadeo con sede en cada ciudad del País.</p>

        <a href="#" class="btn btn-primary">Llamar</a>
      </div>
    </div>
  </div>
</div>
</div>
